// const questions = require('./questions');
const quizContainer = document.getElementById("quiz");
const resultsContainer = document.getElementById("results");
const submitButton = document.getElementById("submit");
const questionCounterText = document.getElementById("questionCounter");
const progressBarFull = document.getElementById("progressBarFull");
const loader = document.getElementById("loader");
const previous = document.getElementById("previous");
const next = document.getElementById("next");
const submit = document.getElementById("submit");
const progressBarBorder = document.getElementById("progressBar");
const goHome = document.getElementById("goHome");



const myQuestions = [
  {
    question: "How Do Browsers Read CSS?",
    answers: {
      A: "From Left To Right",
      B: "From Top To Bottom",
      C: "From Right To Left"
    },
    correctAnswer: "C"
  },
  {
    question: "What Does The 'A' in Ajax mean?",
    answers: {
      A: "Asynchronous",
      B: "Available",
      C: "Accordion"
    },
    correctAnswer: "A"
  },
  {
    question: "What Does 'Stringify' Convert To String?",
    answers: {
      A: "JSONP",
      B: "JSON",
      C: "XML"
    },
    correctAnswer: "B"
  },
  {
    question: "Which Of These Is Not A Back-end Language?",
    answers: {
      A: "C#",
      B: "Python",
      C: "JavaScript"
    },
    correctAnswer: "C"
  },
  {
    question: "Which Of These Support Client-Side Rendering?",
    answers: {
      A: "Back-End",
      B: "Front-End",
      C: "Both Back-End and Front-End"
    },
    correctAnswer: "B"
  },
  {
    question: " '==' Means Equal To While '===' Means ?",
    answers: {
      A: "Almost Equal To",
      B: "Somewhat Equal To",
      C: "Exactly Equal To"
    },
    correctAnswer: "C"
  },
  {
    question: "Who Owns The Map() Method?",
    answers: {
      A: "Strings",
      B: "Arrays",
      C: "Objects"
    },
    correctAnswer: "B"
  },
  {
    question: "What Is Used To Concacenate Strings",
    answers: {
      A: "+",
      B: "||",
      C: "-"
    },
    correctAnswer: "A"
  },
  {
    question: "Which Of These Is A Familiar Word In JavaScript?",
    answers: {
      A: "StrawBerry",
      B: "Chocolate",
      C: "Vanilla"
    },
    correctAnswer: "C"
  },
  {
    question: "Which Of These Is Not A Web Hosting Site?",
    answers: {
      A: "Heroku",
      B: "Jira",
      C: "Netlify"
    },
    correctAnswer: "B"
  }
];
function buildQuiz() {
  // we'll need a place to store the HTML output
  const output = [];
  // for each question...
  myQuestions.forEach((currentQuestion, questionNumber) => {
    //   console.log(questionNumber);
    // we'll want to store the list of answer choices
    // console.log(currentQuestion.answers);
    const answers = [];
    // and for each available answer...
    for (letter in currentQuestion.answers) {
      //    console.log(currentQuestion.answers[letter]);
      // ...add an HTML radio button
      answers.push(
        `<label>
            <input type="radio" name="question${questionNumber}" value="${letter}"> ${letter} :
             ${currentQuestion.answers[letter]}
          </label>`
      );
    }
    // add this question and its answers to the output
    output.push(
      `<div class="slide">
    <div class="question"> ${currentQuestion.question} </div>
    <div class="answers"> ${answers.join("")} </div>
  </div>`
    );
  });
  // console.log(output);
  // finally combine our output list into one string of HTML and put it on the page
  quizContainer.innerHTML = output.join("");
}

(function(){
let myLoader = document.getElementById("loader");
show = function(){
  myLoader.style.display = "block";
  setTimeout(hide, 300); // split seconds
},

hide = function(){
  myLoader.style.display = "none";
};

show();
})();

// display quiz right away
buildQuiz();



let questionCounter = 0;
let currentQuestion = {};
let availableQuestions = [];
// let questions = [...

const maxQuestions = 10;

questionCounter++;

questionCounterText.innerText = `Question  ${questionCounter}/${maxQuestions}`;


const questionIndex = Math.floor(Math.random() * myQuestions.length);

// currentQuestion = availableQuestions[quesetionIndex];

// question.innertext = currentQuestion.question;


function showResults() {
  // gather answer containers from our quiz
  const answerContainers = quizContainer.querySelectorAll(".answers");
  // keep track of user's answers
  let numCorrect = 0;
  // for each question...
  myQuestions.forEach((currentQuestion, questionNumber) => {
    // find selected answer
    const answerContainer = answerContainers[questionNumber];
    const selector = "input[name=question" + questionNumber + "]:checked";

    const userAnswer = (answerContainer.querySelector(selector) || {}).value;
    // if answer is correct
    if (userAnswer === currentQuestion.correctAnswer) {

      // add to the number of correct answers
      numCorrect++;
      // color the answers green
      answerContainers[questionNumber].style.color = "lightgreen";
    }
    // if answer is wrong or blank
    else {
      // color the answers red
      answerContainers[questionNumber].style.color = "orange";
    }
  });

  
let hideQuiz = document.getElementById("quiz").style.display = "none";
let hideQuestionCounter = document.getElementById("questionCounter").style.display = "none";
let hideProgressBar = document.getElementById("progressBarFull").style.display = "none";
let hideprogressBarBorder = document.getElementById("progressBar").style.display = "none";
let hideLoader = document.getElementById("loader").style.display = "none";
let hidePrevious = document.getElementById("previous").style.display = "none";
let hideNext = document.getElementById("next").style.display = "none";
let hideSubmit = document.getElementById("submit").style.display = "none";

// let showGoHome = document.getElementById("goHome").style.display = "block";
  // show number of correct answers out of total
  resultsContainer.innerHTML = `<h4>WELL DONE!</h4> You Scored ${numCorrect}   Out Of  ${myQuestions.length}`;
   resultsContainer.innerHTML += '<div id="goHomeDiv"><a id="goHome" class="btn btnHome" href="">Go Home</a></div>';
}


// on submit, show results
submitButton.addEventListener("click", showResults);


/* PAGINATION */
const previousButton = document.getElementById("previous");
const nextButton = document.getElementById("next");
const slides = document.querySelectorAll(".slide");
let currentSlide = 0;
function showSlide(n) {
  console.log("Next");
  slides[currentSlide].classList.remove('active-slide');
  slides[n].classList.add("active-slide");
  currentSlide = n;
  if (currentSlide === 0) {
    previousButton.style.display = "none";
  } else {
    previousButton.style.display = "inline-block";
  }

  if (currentSlide === slides.length - 1) {
    nextButton.style.display = "none";
    submitButton.style.display = "inline-block";
  } else {
    nextButton.style.display = "inline-block";
    submitButton.style.display = "none";
  }
}
showSlide(0);

function showNextSlide() {
  questionCounter++;
  console.log("Question Counter " + questionCounter);
  questionCounterText.innerText = `Question  ${questionCounter}/${maxQuestions}`;

  //Update progress bar
progressBarFull.style.width = `${(questionCounter / maxQuestions) * 100}%`;
  showSlide(currentSlide + 1);
  
  (function(){
    let myLoader = document.getElementById("loader");
    show = function(){
      myLoader.style.display = "block";
      setTimeout(hide, 300); // split seconds
    },
    
    hide = function(){
      myLoader.style.display = "none";
    };
    
    show();
    })();
  

  
  
}
function showPreviousSlide() {
  questionCounter--;
  console.log("Question Counter " + questionCounter);
  questionCounterText.innerText = `Question  ${questionCounter}/${maxQuestions}`;

  //Update progress bar
progressBarFull.style.width = `${(questionCounter / maxQuestions) * 100}%`;
  showSlide(currentSlide - 1);

  (function(){
    let myLoader = document.getElementById("loader");
    show = function(){
      myLoader.style.display = "block";
      setTimeout(hide, 300); // split seconds
    },
    
    hide = function(){
      myLoader.style.display = "none";
    };
    
    show();
    })();
}
previousButton.addEventListener("click", showPreviousSlide);
nextButton.addEventListener("click", showNextSlide);